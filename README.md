# **Program to generate Fibonacci Series in C++** 

## Implementation of the Fibonacci Heap data structure for the course Algorithms on Discrete Structures.The Fibonacci sequence is a set of numbers that starts with a one or a zero, followed by a one, and proceeds based on the rule that each number (called a Fibonacci number) is equal to the sum of the preceding two numbers.

Johnna Grannan - CEO  at  http://www.beyonddietcentral.com 

**# Logic of the Program #** 

### First you should know how Fibonacci series  is calculated in mathematics and then try to implement in c++.
Now, if you noticed the series carefully, the first two numbers i.e. 0,1 are displayed as it is and then each subsequent number is calculated by adding the previous two numbers.


#  Source Code : #
/*
 
    fibonacci.cpp
    C++ Program to Generate Fibonacci Series
    To compile (Need g++ compiler for unix/linux or Microsoft compiler for windows)
    g++ fibonacci.cpp -o  fibo
    To exectue.
    ./fibo
 
    For windows
    c:\directory> cl -GX fibonacci.cpp

**# SnapShots #**

![c program to generate fibonacci series using Function.jpg](https://bitbucket.org/repo/gp45RX/images/3840854493-c%20program%20to%20generate%20fibonacci%20series%20using%20Function.jpg)